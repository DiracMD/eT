


                     eT 1.8 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, R. Matveeva, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, 
   E. Ronca, M. Scavino, A. K. Schnack-Petersen, A. S. Skeidsvoll, Å. 
   H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.8.0 Ibiza
  ------------------------------------------------------------
  Configuration date: 2022-06-30 08:43:06 UTC +02:00
  Git branch:         release-v1.8.0
  Git hash:           9204bbd8a5d9984444afd222ec28d0f1c1e17adf
  Fortran compiler:   GNU 9.4.0
  C compiler:         GNU 9.4.0
  C++ compiler:       GNU 9.4.0
  LAPACK type:        MKL
  BLAS type:          MKL
  64-bit integers:    ON
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: cs
        charge: 0
        multiplicity: 2
     end system

     do
        ground state
     end do

     method
        uhf
     end method

     memory
        available: 8
     end memory

     solver scf
       algorithm:          scf-diis
       energy threshold:   1.0d-10
       gradient threshold: 1.0d-10
     end solver scf


  Calculation start:2022-06-30 08:45:19 UTC +02:00

  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: UHF wavefunction
  ===================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: 3-21g
        1 Cs     0.000000000000     0.000000000000     0.000000000000        1
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: 3-21g
        1 Cs     0.000000000000     0.000000000000     0.000000000000        1
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               37
     Number of orthonormal atomic orbitals:   37

  - Molecular orbital details:

     Number of alpha electrons:              28
     Number of beta electrons:               27
     Number of virtual alpha orbitals:        9
     Number of virtual beta orbitals:        10
     Number of molecular orbitals:           37


  Generating initial SAD density
  ==============================


  Determining reference state
  ===========================

  - Setting initial AO density to sad

     Energy of initial guess:             -7530.568190922048
     Number of electrons in guess:           55.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-15
     Exchange screening threshold:   0.1000E-13
     ERI cutoff:                     0.1000E-15
     One-electron integral  cutoff:  0.1000E-20
     Cumulative Fock threshold:      0.1000E+01

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1         -7530.542527047885     0.4281E-01     0.7531E+04
     2         -7530.543242039055     0.1751E-02     0.7150E-03
     3         -7530.543343909521     0.6922E-03     0.1019E-03
     4         -7530.543363599996     0.6722E-04     0.1969E-04
     5         -7530.543363805165     0.7224E-05     0.2052E-06
     6         -7530.543363806802     0.3314E-05     0.1637E-08
     7         -7530.543363807133     0.2769E-06     0.3311E-09
     8         -7530.543363807135     0.1487E-07     0.2728E-11
     9         -7530.543363807136     0.2639E-09     0.9095E-12
    10         -7530.543363807137     0.4195E-10     0.9095E-12
  ---------------------------------------------------------------
  Convergence criterion met in 10 iterations!

  - Summary of UHF wavefunction energetics (a.u.):

     HOMO-LUMO gap (alpha):          0.289543124727
     HOMO-LUMO gap (beta):           0.696523627897
     Nuclear repulsion energy:       0.000000000000
     Electronic energy:          -7530.543363807137
     Total energy:               -7530.543363807137

  - UHF wavefunction spin expectation values:

     Sz:                   0.50000000
     Sz(Sz + 1):           0.75000000
     S^2:                  0.75022793
     Spin contamination:   0.00022793

  Peak memory usage during the execution of eT: 492.200 KB

  Total wall time in eT (sec):              3.45012
  Total cpu time in eT (sec):               6.86726

  Calculation end:2022-06-30 08:45:23 UTC +02:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713

  eT terminated successfully!
