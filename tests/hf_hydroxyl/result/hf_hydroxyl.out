


                     eT 1.8 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, R. Matveeva, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, 
   E. Ronca, M. Scavino, A. K. Schnack-Petersen, A. S. Skeidsvoll, Å. 
   H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.8.0 Ibiza
  ------------------------------------------------------------
  Configuration date: 2022-06-30 08:43:06 UTC +02:00
  Git branch:         release-v1.8.0
  Git hash:           9204bbd8a5d9984444afd222ec28d0f1c1e17adf
  Fortran compiler:   GNU 9.4.0
  C compiler:         GNU 9.4.0
  C++ compiler:       GNU 9.4.0
  LAPACK type:        MKL
  BLAS type:          MKL
  64-bit integers:    ON
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: oh-
        charge: -1
        multiplicity: 1
     end system

     do
        ground state
     end do

     method
        hf
     end method

     memory
        available: 8
     end memory

     solver scf
       algorithm:          scf-diis
       energy threshold:   1.0d-10
       gradient threshold: 1.0d-10
     end solver scf


  Calculation start:2022-06-30 08:45:04 UTC +02:00

  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ===================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: 3-21g
        1  O     0.000000000000     0.000000000000     0.000000000000        1
        2  H     0.000000000000     0.000000000000     0.800000000000        2
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: 3-21g
        1  O     0.000000000000     0.000000000000     0.000000000000        1
        2  H     0.000000000000     0.000000000000     1.511780899652        2
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               11
     Number of orthonormal atomic orbitals:   11

  - Molecular orbital details:

     Number of occupied orbitals:         5
     Number of virtual orbitals:          6
     Number of molecular orbitals:       11


  Generating initial SAD density
  ==============================


  Determining reference state
  ===========================

  - Setting initial AO density to sad

  Warning: mismatch in number of electrons from initial guess. It can 
           be beneficial to assign charges to atoms.

     Energy of initial guess:               -74.800893188501
     Number of electrons in guess:            9.000000000000
     Overall charge:                                      -1

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-15
     Exchange screening threshold:   0.1000E-13
     ERI cutoff:                     0.1000E-15
     One-electron integral  cutoff:  0.1000E-20
     Cumulative Fock threshold:      0.1000E+01

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -74.716243720030     0.1961E+00     0.7472E+02
     2           -74.797707312304     0.9260E-01     0.8146E-01
     3           -74.815146943062     0.3305E-02     0.1744E-01
     4           -74.815298949693     0.1093E-02     0.1520E-03
     5           -74.815315485048     0.9298E-04     0.1654E-04
     6           -74.815315619359     0.9509E-05     0.1343E-06
     7           -74.815315619820     0.1079E-05     0.4612E-09
     8           -74.815315619823     0.9798E-07     0.3212E-11
     9           -74.815315619823     0.4290E-08     0.1421E-13
    10           -74.815315619823     0.2737E-09     0.2842E-13
    11           -74.815315619823     0.2805E-10     0.2842E-13
  ---------------------------------------------------------------
  Convergence criterion met in 11 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.648960723281
     Nuclear repulsion energy:       5.291772109200
     Electronic energy:            -80.107087729023
     Total energy:                 -74.815315619823

  :: There was 1 warning during the execution of eT. ::

  Peak memory usage during the execution of eT: 28.096 KB

  Total wall time in eT (sec):              0.06060
  Total cpu time in eT (sec):               0.09826

  Calculation end:2022-06-30 08:45:04 UTC +02:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713

  eT terminated successfully!
