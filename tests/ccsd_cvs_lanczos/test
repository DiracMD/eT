#!/usr/bin/env python3

from pathlib import Path
import sys

# we make sure we can import runtest and runtest_config
sys.path.append(str(Path(__file__).parents[1]))

# we import essential functions from the runtest library
from runtest import version_info, get_filter, cli, run

# this tells runtest how to run your code
from runtest_config import configure

# we stop the script if the major version is not compatible
assert version_info.major == 2

# construct filter(s)
f = [
    get_filter(string='Final ground state energy (a.u.):',
               abs_tolerance=1.0e-10),

    get_filter(from_string='State.      energy [a.u]         energy [eV]         Osc. strength',
               num_lines=11,
               abs_tolerance=1.0e-10,
               mask=[2])
]

checkfiles = ["ccsd_cvs_lanczos.lanczos30_X",
              "ccsd_cvs_lanczos.lanczos30_Y",
              "ccsd_cvs_lanczos.lanczos30_Z"]

test_dir = Path(__file__).parent
checkfiles = [test_dir / i for i in checkfiles]

# invoke the command line interface parser which returns options
options = cli()

ierr = 0
for inp in ['ccsd_cvs_lanczos.inp']:
    # the run function runs the code and filters the outputs
    ierr += run(options,
                configure,
                input_files=inp,
                filters={'out': f})

missing_files = [i for i in checkfiles if not i.exists()]
filerror = len(missing_files)

if filerror > 0:
    print("Missing files:")
    print("\n".join([i.name for i in missing_files]))

sys.exit(ierr + filerror)
