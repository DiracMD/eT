#!/usr/bin/env python3

from pathlib import Path
import sys

# we make sure we can import runtest and runtest_config
sys.path.append(str(Path(__file__).parents[1]))

# we import essential functions from the runtest library
from runtest import version_info, cli, run

# this tells runtest how to run your code
from runtest_config import configure

# functions to get the filters
from filters import get_mlhf_filter

# we stop the script if the major version is not compatible
assert version_info.major == 2

# construct filter(s)
threshold = 1.0e-10
f = get_mlhf_filter(threshold, convergence=True, restart=True)

# invoke the command line interface parser which returns options
options = cli()

ierr = 0

# Run the first mlhf calculation with a maximum number of iterations
# equal to 5:
# - extra arguments -ks and --scratch to keep the scratch 
#   after the first calculation
# - we are not checking the output on the first calculation;
#   to do so, add the "filters" line
#
for inp in ['mlhf_5_iterations.inp']:
    #
    # the run function runs the code with extra arguments
    # keep the scratch in the tests folder in build
    #
    ierr += run(options,
                configure,
                input_files=inp,
                extra_args='-ks --scratch ./scratch')

# reset ierr: helpful if the first calculation is supposed to crash
# e.g. if the calculations has to little iterations to converge
ierr = 0

#
# Run the second mlhf calculation with the keyword "restart"
# and without a fixed maximum number of iterations
#
for inp in ['restart_mlhf.inp']:
    #
    # the run function runs the code and filters the outputs
    # don't provide -ks to delete scratch after succesful run
    #
    ierr += run(options,
                configure,
                input_files=inp,
                extra_args='--scratch ./scratch',
                filters={'out': f})

sys.exit(ierr)
